var quotes_en = [
    {
        "quote": "What a disappointment, because I like to win.",
        "author": "Steve Wozniak"
    },{
        "quote": "So why did the Apple III have so many problems, despite the fact that all of our other products had worked so great?<br>\
I can answer that. It's because the Apple III was not developed by a single engineer or a couple of engineers working together.<br>\
It was developed by a committee, by the marketing departement.",
        "author": "Steve Wozniak"
    },{
        "quote": "Most days I wake up thinking I'm the luckiest bastard alive.",
        "author": "Linus Torvalds"
    },{
        "quote": "Remember that, back then [1992], people who liked Windows were not on the Internet.",
        "author": "Linus Torvalds"
    },{
        "quote": "At on point I felt I wasn't getting enough exercise, so I decided to ride my bycicle the six miles between our apartment and Transmeta's headquarters. It was on a Monday. There were not hills to climb, but a strong wind blew in the wrong direction, making it more challenging than I wanted. By the time I left work ten hours later, the wind had shifted so that it was still in the wrong direction. I phoned Tove and she picked me up.<br>\
Needless to say, biking-to-work didn't happen again.",
        "author": "Linus Torvalds"
    },
    {
        "quote": "From then on I realized that for the most part I had no crashes at all, and the only thing I'd changed about my system was I'd stopped using Internet Explorer.",
        "author": "Steve Wozniak"
    },{
        "quote": "Court: When was the first time you met in real life?<br>\
Gottfrid Svartholm: We don't use the expression \"in real life\". We say \"Away from Keyboard\". We think the internet is for real."
    },{
        "quote": "If there were a modesty Olympics, I'd probably get a bronze medal.",
        "author": "TheAustr0naut"
    }

];
