window.onload = function() {
    setTime();
    window.setInterval(setTime, 1000*60); /* refresh interval in milliseconds */
    setDate();
    setQuote();
};


function setTime() {
    document.getElementById('time').innerHTML = timeNow();
}

function timeNow() {
  var d = new Date(),
      h = ( d.getHours() < 10 ? '0' : '' ) + d.getHours(),
      m = ( d.getMinutes() < 10 ? '0' : '' ) + d.getMinutes();
    return formatTime( d.getHours(), d.getMinutes(), d.getSeconds() );
}

function formatTime(h, m, s) {
    var str = '';
    /* prepend zeros if necessary */
    h = ( h < 10 ? '0' : '' ) + h;
    m = ( m < 10 ? '0' : '' ) + m;
    s = ( s < 10 ? '0' : '' ) + s;

    str = h + ' ' + m;

    return str;
}

function setDate() {
    var d = new Date(),
        y = d.getFullYear(),
        // m = d.getMonth()+1, /* numeric month */
        m = d.toLocaleString("en-US", { month: "long" }); /* literal month */
        t = d.getDate();
    document.getElementById('date').innerHTML = t + '. ' + m + ' ' + y;
}

function setQuote() {
    var quotes = quotes_en;

    if ( typeof quotes === 'undefined' ) {
        console.log("Error: no quotes array found");
        return;
    }

    var q = quotes[ randomIndex(quotes.length) ];

    document.getElementById("quote_content").innerHTML = '„ ' + q.quote + ' ”';
    document.getElementById("quote_author").innerHTML =
        ( typeof q.author === 'undefined' ) ? '' : '— ' + q.author;
}

function randomIndex(len) {
    return Math.floor( Math.random() * len );
}

function interpretSearch() {
    var search = document.getElementById("js-search-input").value.split(" ");
    var searchTerms = "";
    console.log(search[0])
    switch(search[0])
    {
        case '?':
            alert("t: twitter\ntw: twitch\nr: reddit\ny: youtube\na: amazon");
            break;
        case 't:':
            window.location.href = "https://twitter.com/" + search[1];
            return false;
        case 'tw:':
            window.location.href = "https://twitch.tv/" + search[1];
            return false;
        case 'r:':
            window.location.href = "https://reddit.com/r/" + search[1];
            return false;
        case 'y:':
            for(var i = 1; i < search.length; i++)
            {
                searchTerms += search[i]+'+';
            }
            window.location.href = "https://youtube.com/results?search_query=" + searchTerms;
            return false;
        case 'a:':
            for(var i = 1; i < search.length; i++)
            {
                searchTerms += search[i]+'+';
            }
            window.location.href = "https://www.amazon.com/s/ref=nb_sb_noss_2?url=search-alias%3Daps&field-keywords=" + searchTerms;
            return false;
        default:
            for(var i = 0; i < search.length; i++)
            {
                searchTerms += search[i]+'+';
            };
            window.location.href = "https://google.com/search?q="+searchTerms;
            return false;
    }
};
